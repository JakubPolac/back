import {Response, Request} from "express";
import {Todo} from "../models/todo";

export class ActionRouterDeleteUser {
    public async routerDeleteAction(req: Request, res: Response): Promise<Response>{
        const idToDel = req.params.id;

        try{
            const waitForDelete = await Todo.deleteOne( { _id: idToDel })
            console.log("Deleting user with ID: ",idToDel);

            return res.json("successful");
        } catch (err){
            return res.json("error");
        }
    }
}