import * as express from "express";
import { Request, Response } from 'express';
import {ActionRouterUserGetter} from '../actions/actions_getUsers';
import {ActionRouterRegisterUser} from '../actions/actions_registerUser';
import {ActionRouterDeleteUser} from '../actions/actions_delUser';
import {ActionRouterUpdateUser} from "../actions/actions_updateUser";
import {ActionRouterGetUserByID} from "../actions/actions_getUserByID";

const cors = require('cors')

const router = express.Router();
router.use(cors());
const actionUserGetter: ActionRouterUserGetter = new ActionRouterUserGetter();
const actionRegisterUser: ActionRouterRegisterUser = new ActionRouterRegisterUser();
const actionDeleteUser: ActionRouterDeleteUser = new ActionRouterDeleteUser();
const actionUpdateUser:ActionRouterUpdateUser = new ActionRouterUpdateUser();
const actionGetUser:ActionRouterGetUserByID = new ActionRouterGetUserByID();

router.get('/api/users', (req: Request, res:Response) => actionUserGetter.routerUserGetter(req,res));

router.get('/api/user/:id', (req: Request, res:Response) => actionGetUser.routerGetUserByID(req,res));

router.post('/api/user-new', (req: Request, res:Response)  => actionRegisterUser.routerRegisterAction(req,res));

router.delete('/api/user-del/:id', (req: Request, res: Response) => actionDeleteUser.routerDeleteAction(req,res));

router.patch('/api/user-edit', (req: Request, res: Response) => actionUpdateUser.routerUpdateAction(req,res));

export {router}
