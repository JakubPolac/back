import {Response, Request} from "express";
import {Todo} from "../models/todo";

export class ActionRouterUserGetter {
    public async routerUserGetter(req: Request, res: Response){
        let todos = await Todo.find({});
        return res.json(todos);
    }
}