import {Response, Request} from "express";
import {Todo} from "../models/todo";

export class ActionRouterUpdateUser {
    public async routerUpdateAction(req: Request, res: Response): Promise<Response>{
        const idToEdit = req.body.id;
        const newName = req.body.name;
        const newUsername = req.body.username;
        const newEmail = req.body.email;
        const newPhone = req.body.phone;
        const newWebsite = req.body.website;

        try{
            const waitForEdit = await Todo.updateOne( { _id: idToEdit }, {
                name: newName,
                username: newUsername,
                email: newEmail,
                phone: newPhone,
                website: newWebsite
                });
            console.log("Editing user with ID: ",idToEdit);

            return res.json("successful");
        } catch (err){
            return res.json("error");
        }
    }
}