import {Response, Request} from "express";
import {Todo} from "../models/todo";

export class ActionRouterGetUserByID {
    public async routerGetUserByID(req: Request, res: Response){
        const idToGet = req.params.id;

        try{
            const waitForGet = await Todo.find( { _id: idToGet })
            console.log("Getting user with ID: ",idToGet);

            return res.json(waitForGet);
        } catch (err){
            return res.json("error");
        }
    }
}