import {Response, Request} from "express";
import {Todo} from "../models/todo";

export class ActionRouterRegisterUser {
    public async routerRegisterAction(req: Request, res: Response):Promise<Response>{
        const name = (req.body.name).toString();
        const username = (req.body.username).toString();
        const email = (req.body.email).toString();
        const phone = (req.body.phone);
        const website = (req.body.website).toString();

        //const searchForExistingID = Todo.find({ id: { $in: [ req.body.id ] } });
        //if(await searchForExistingID.count() > 0) { return; }

        try {
            const newUser = await Todo.build({name, username, email, phone, website});
            console.log(newUser);

            await newUser.save();
            return res.json("successful");
        } catch (err){
            res.json("error");
            return res.sendStatus(400);
        }
    }
}
