import express from 'express';
import mongoose from 'mongoose';
import bodyParser, {json} from 'body-parser';
import {router} from "./routes/todo";
const cors = require('cors')

const app = express();
app.use(json());
app.use(express.urlencoded({ extended: false }));
app.use(router);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

mongoose.connect('mongodb://localhost:27017/api', {
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true
})
    .then(() => {
        console.log('connection');
    }).catch(err => {
        console.log(err)
    })

app.listen(3000, () => {
    console.log(`Server listening at 3000`);
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/index_form.html");
});

