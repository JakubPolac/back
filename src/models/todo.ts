import mongoose from 'mongoose';

interface ITodo{
    name: ToDoDoc['name'];
    username: ToDoDoc['username'];
    email: ToDoDoc['email'];
    phone: ToDoDoc['phone'];
    website: ToDoDoc['website'];
}
//This interface will tell typescript that we will have a build function in the Todo model
interface todoModelInterface extends mongoose.Model<ToDoDoc>{
    build(attr:ITodo): ToDoDoc
}

interface ToDoDoc extends mongoose.Document{
    name: string;
    username: string;
    email: string;
    phone: number;
    website: string;
}

const todoSchema = new mongoose.Schema({
    name:{ type:String, required:true },
    username:{ type:String, required:true },
    email:{ type:String, required:true },
    phone:{ type:Number, required:true },
    website:{ type:String, required:true }
});

//przypisanie statycznej funkcji do todoSchema
todoSchema.statics.build = (attr: ITodo) => {
    return new Todo(attr);
}

const Todo = mongoose.model<ToDoDoc, todoModelInterface>('Todo', todoSchema, 'Todo');

export{Todo}